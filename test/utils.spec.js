const test = require('ava');
const { generateValues } = require('./helper')
const { isObjectWithKeys, validRangeBounds, isValueInBounds } = require('../app/utils');


test('isObjectWithKeys (correct objects)', t => {
  const testValueObjects = generateValues({key: 'value'})
  testValueObjects.forEach(elem => {
    const result_false = isObjectWithKeys(elem.value)
    t.true(result_false, elem.msg);
  });
})

test('isObjectWithKeys (incorrect objects)', t => {
  const testValueObjects = generateValues(null, undefined, 'str', 100, true, {}, [], [{key: 'value'}], () => 'testing')
  testValueObjects.forEach(elem => {
    const result_false = isObjectWithKeys(elem.value)
    t.false(result_false, elem.msg);
  });
})

test('validRangeBounds (correct values)', t => {
  t.true(validRangeBounds(null, undefined));
  t.true(validRangeBounds(undefined, null));
  t.true(validRangeBounds(undefined, 1));
  t.true(validRangeBounds(1, undefined));
  t.true(validRangeBounds(1, 1));
  t.true(validRangeBounds(1, 10));
})

test('validRangeBounds (incorrect values)', t => {
  t.false(validRangeBounds(10, 1));
  t.false(validRangeBounds('1', 1));
  t.false(validRangeBounds(1, '1'));
  t.false(validRangeBounds(true, []));
  t.false(validRangeBounds([], true));
  t.false(validRangeBounds([], {}));
  t.false(validRangeBounds({}, []));
  t.false(validRangeBounds(() => true, () => false));
})

test('isValueInBounds (positive values)', t => {
  const min = 3, max = 7;
  t.false(isValueInBounds(2, min, max, true, false));
  t.true (isValueInBounds(3, min, max, true, false));
  t.true (isValueInBounds(4, min, max, true, false));
  t.false(isValueInBounds(8, min, max, true, false));

  t.false(isValueInBounds(2, min, max, false, true));
  t.true (isValueInBounds(4, min, max, false, true));
  t.true (isValueInBounds(7, min, max, false, true));
  t.false(isValueInBounds(8, min, max, false, true));
})

test('isValueInBounds (negative values)', t => {
  const min = -7, max = -3;
  t.false(isValueInBounds(-8, min, max, true, false));
  t.true (isValueInBounds(-7, min, max, true, false));
  t.true (isValueInBounds(-4, min, max, true, false));
  t.false(isValueInBounds(0, min, max, true, false));

  t.false(isValueInBounds(-8, min, max, false, true));
  t.true (isValueInBounds(-5, min, max, false, true));
  t.true (isValueInBounds(-3, min, max, false, true));
  t.false(isValueInBounds(0, min, max, false, true));
})