const test = require('ava')
const { generateValues } = require('./helper')
const rules = require('../app/index')

function translateFn(localeKey) {
  return localeKey
}

test.before(t => {
  const options = {
    integer: 'locale_integer',
    integerNeg: 'locale_integer_neg',
    integerNotNeg: 'locale_integer_not_neg',
    double: 'locale_double',
    doubleNeg: 'localle_double_neg',
    doubleNotNeg: 'localle_double_not_neg',
    required: 'locale_key_required',
    email: 'locale_key_email',
    percent: 'locale_key_percent',
    number: 'locale_key_number',
    amount: 'locale_key_amount',
    latinCode: 'locale_key_latinCode',
    passwordLength: 'locale_key_passwordLength',
    notEqualToNew: 'locale_key_notEqualToNew',
    passwordNotStrength: 'locale_key_passwordNotStrength',
    descrip_en_de: 'locale_descrip_en_de',
    descripEnDe: 'locale_descripEnDe',
    // nickname: 'locale_nickname'
  }
  t.context.rules = rules(translateFn, options)
})

test('integer (satisfied)', t => {
  const testValueObjects = generateValues('-0', '-00', '-010', '-1', '-9', '-102', '0', '00', '010', '1', '9', '102')
  testValueObjects.forEach(elem => {
    const result = t.context.rules.integer()(elem.value)
    t.is(typeof result, 'boolean', elem.msg)

    const result_range_min = t.context.rules.integer({ minVal: -200 })(elem.value)
    t.is(typeof result_range_min, 'boolean', elem.msg)

    const result_range_max = t.context.rules.integer({ maxVal: 200 })(elem.value)
    t.is(typeof result_range_max, 'boolean', elem.msg)

    const result_interval_close_open = t.context.rules.integer({ minVal: -102, maxVal: 200, interval: [true, false] })(
      elem.value
    )
    t.is(typeof result_interval_close_open, 'boolean', elem.msg)

    const result_interval_open_close = t.context.rules.integer({ minVal: -200, maxVal: 102, interval: [false, true] })(
      elem.value
    )
    t.is(typeof result_interval_open_close, 'boolean', elem.msg)
  })
})

test('integer (not satisfied)', t => {
  const valueObjects = generateValues('-', '--0', '--1', '--9', '--102', '0-', '1-', '9-', '102-', '0a', 'a', '1a0')
  valueObjects.forEach(elem => {
    const result = t.context.rules.integer()(elem.value)
    t.is(typeof result, 'string', elem.msg)
  })
})

test('integerNeg (satisfied)', t => {
  const valueObjects = generateValues('-0010', '-1', '-10', '-100')
  valueObjects.forEach(elem => {
    const result = t.context.rules.integerNeg()(elem.value)
    t.is(typeof result, 'boolean', elem.msg)

    const result_range_min = t.context.rules.integerNeg({
      minVal: -100,
      interval: [true, false],
    })(elem.value)
    t.is(typeof result_range_min, 'boolean', elem.msg)

    const result_range_max = t.context.rules.integerNeg({
      maxVal: 100,
    })(elem.value)
    t.is(typeof result_range_max, 'boolean', elem.msg)
  })
})

test('integerNeg (not satisfied)', t => {
  const valueObjects = generateValues('-', '-0', '0', '000', '0010', '1', '10')
  valueObjects.forEach(elem => {
    const result = t.context.rules.integerNeg()(elem.value)
    t.is(typeof result, 'string', elem.msg)

    const result_range_min = t.context.rules.integerNeg({
      minVal: -100,
      interval: [true, false],
    })(elem.value)
    t.is(typeof result_range_min, 'string', elem.msg)

    const result_range_max = t.context.rules.integerNeg({
      maxVal: 100,
    })(elem.value)
    t.is(typeof result_range_max, 'string', elem.msg)
  })
})

test('integerNotNeg (satisfied)', t => {
  const valueObjects = generateValues('0', '-0', '0000', '-0000', '0010', '1', '10', '100')
  valueObjects.forEach(elem => {
    const result = t.context.rules.integerNotNeg()(elem.value)
    t.is(typeof result, 'boolean', elem.msg)

    const result_range_min = t.context.rules.integer({ minVal: -200 })(elem.value)
    t.is(typeof result_range_min, 'boolean', elem.msg)

    const result_range_max = t.context.rules.integer({ maxVal: 200 })(elem.value)
    t.is(typeof result_range_max, 'boolean', elem.msg)

    const result_interval_close_open = t.context.rules.integerNotNeg({
      minVal: 0,
      maxVal: 200,
      interval: [true, false],
    })(elem.value)
    t.is(typeof result_interval_close_open, 'boolean', elem.msg)

    const result_interval_open_close = t.context.rules.integerNotNeg({
      minVal: -1,
      maxVal: 100,
      interval: [false, true],
    })(elem.value)
    t.is(typeof result_interval_open_close, 'boolean', elem.msg)
  })
})

test('integerNotNeg (not satisfied)', t => {
  const valueObjects = generateValues('-', '-0010', '-1', '-10', '-100')
  valueObjects.forEach(elem => {
    const result = t.context.rules.integerNotNeg()(elem.value)
    t.is(typeof result, 'string', elem.msg)
  })
})

test('double (satisfied)', t => {
  const testValueObjects = generateValues(
    '-0',
    '-00',
    '-001',
    '-0.0',
    '-0.25',
    '-1',
    '-10.25',
    '0',
    '00',
    '001',
    '0.0',
    '0.25',
    '1',
    '10.25'
  )
  testValueObjects.forEach(elem => {
    const result = t.context.rules.double()(elem.value)
    t.is(typeof result, 'boolean', elem.msg)

    const result_range_min = t.context.rules.double({ minVal: -20 })(elem.value)
    t.is(typeof result_range_min, 'boolean', elem.msg)

    const result_range_max = t.context.rules.double({ maxVal: 20 })(elem.value)
    t.is(typeof result_range_max, 'boolean', elem.msg)

    const result_interval_close_open = t.context.rules.double({ minVal: -10.25, maxVal: 20, interval: [true, false] })(
      elem.value
    )
    t.is(typeof result_interval_close_open, 'boolean', elem.msg)

    const result_interval_open_close = t.context.rules.double({ minVal: -20, maxVal: 10.25, interval: [false, true] })(
      elem.value
    )
    t.is(typeof result_interval_open_close, 'boolean', elem.msg)
  })
})

test('double (not satisfied)', t => {
  const valueObjects = generateValues(
    '',
    '--0',
    '--1',
    '--9',
    '--102',
    '0-',
    '1-',
    '9-',
    '102-',
    '0a',
    'a',
    '1a0',
    '-',
    '-0.',
    '0.',
    '00.',
    '001.',
    '0.0.',
    '.0',
    '.25',
    '10.'
  )
  valueObjects.forEach(elem => {
    const result = t.context.rules.double()(elem.value)
    t.is(typeof result, 'string', elem.msg)
  })
})
test('doubleNeg (satisfied)', t => {
  const valueObjects = generateValues('-0010', '-0010.25', '-1', '-1.25', '-10', '-10.25', '-100')
  valueObjects.forEach(elem => {
    const result = t.context.rules.doubleNeg()(elem.value)
    t.is(typeof result, 'boolean', elem.msg)

    const result_range_min = t.context.rules.doubleNeg({
      minVal: -100,
      interval: [true, false],
    })(elem.value)
    t.is(typeof result_range_min, 'boolean', elem.msg)

    const result_range_max = t.context.rules.doubleNeg({
      maxVal: 100,
    })(elem.value)
    t.is(typeof result_range_max, 'boolean', elem.msg)
  })
})

test('doubleNeg (not satisfied)', t => {
  const valueObjects = generateValues(
    '',
    '-',
    '-0',
    '0',
    '000',
    '0.25',
    '.25',
    '0010',
    '0010.25',
    '1',
    '1.25',
    '10',
    '10.25'
  )
  valueObjects.forEach(elem => {
    const result = t.context.rules.doubleNeg()(elem.value)
    t.is(typeof result, 'string', elem.msg)

    const result_range_min = t.context.rules.doubleNeg({
      minVal: -100,
      interval: [true, false],
    })(elem.value)
    t.is(typeof result_range_min, 'string', elem.msg)

    const result_range_max = t.context.rules.doubleNeg({
      maxVal: 100,
    })(elem.value)
    t.is(typeof result_range_max, 'string', elem.msg)
  })
})

test('doubleNotNeg (satisfied)', t => {
  const valueObjects = generateValues('0', '-0', '0000', '-0000', '0.0', '0.25', '0010', '1', '1.10', '10', '10.250')
  valueObjects.forEach(elem => {
    const result = t.context.rules.doubleNotNeg()(elem.value)
    t.is(typeof result, 'boolean', elem.msg)

    const result_range_min = t.context.rules.doubleNotNeg({ minVal: 0, interval: [true, false] })(elem.value)
    t.is(typeof result_range_min, 'boolean', elem.msg)

    const result_range_max = t.context.rules.doubleNotNeg({ maxVal: 100 })(elem.value)
    t.is(typeof result_range_max, 'boolean', elem.msg)
  })
})

test('doubleNotNeg (not satisfied)', t => {
  const valueObjects = generateValues('', '-', '-.25', '.25', '-0010', '-0010.25', '-1', '-10.25', '-100')
  valueObjects.forEach(elem => {
    const result = t.context.rules.doubleNotNeg()(elem.value)
    t.is(typeof result, 'string', elem.msg)
  })
})
test('minNumber (satisfied)', t => {
  const valueObjects = generateValues(25, 10, null, 10.01)
  valueObjects.forEach(elem => {
    const result = t.context.rules.numberMin({
      value: 10,
    })(elem.value)
    t.is(typeof result, 'boolean', elem.msg)
  })
})

test('minNumber (not satisfied)', t => {
  const valueObjects = generateValues(5, 9.99)
  valueObjects.forEach(elem => {
    const result = t.context.rules.numberMin({
      value: 10,
    })(elem.value)
    t.is(typeof result, 'string', elem.msg)
  })
})

test('maxNumber (satisfied)', t => {
  const valueObjects = generateValues(25, 100, null, 99.9)
  valueObjects.forEach(elem => {
    const result = t.context.rules.numberMax({
      value: 100,
    })(elem.value)
    t.is(typeof result, 'boolean', elem.msg)
  })
})

test('maxNumber (not satisfied)', t => {
  const valueObjects = generateValues(125, 100.01)
  valueObjects.forEach(elem => {
    const result = t.context.rules.numberMax({
      value: 100,
    })(elem.value)
    t.is(typeof result, 'string', elem.msg)
  })
})

test('required (satisfied)', t => {
  const valueObjects = generateValues('test', ' ')
  valueObjects.forEach(elem => {
    const result = t.context.rules.required(elem.value)
    t.is(typeof result, 'boolean', elem.msg)
  })
})

test('required (not satisfied)', t => {
  const valueObjects = generateValues(null, undefined, '')
  valueObjects.forEach(elem => {
    const result = t.context.rules.required(elem.value)
    t.is(typeof result, 'string', elem.msg)
  })
})

test('requiredACRecIds (satisfied)', t => {
  const valueObjects = generateValues(null, 10)
  valueObjects.forEach(elem => {
    const result = t.context.rules.requiredACRecIds(elem.value)
    t.is(typeof result, 'boolean', elem.msg)
  })
})

test('requiredACRecIds (not satisfied)', t => {
  const valueObjects = generateValues(undefined) // null is value of 'ALL'
  valueObjects.forEach(elem => {
    const result = t.context.rules.requiredACRecIds(elem.value)
    t.is(typeof result, 'string', elem.msg)
  })
})

test('requiredMultiAC (satisfied)', t => {
  const valueObjects = generateValues([0], [0, 2])
  valueObjects.forEach(elem => {
    const result = t.context.rules.requiredMultiAC(elem.value)
    t.is(typeof result, 'boolean', elem.msg)
  })
})

test('requiredMultiAC (not satisfied)', t => {
  const valueObjects = generateValues(null, [])
  valueObjects.forEach(elem => {
    const result = t.context.rules.requiredMultiAC(elem.value)
    t.is(typeof result, 'string', elem.msg)
  })
})

test('email (satisfied)', t => {
  const valueObjects = generateValues('d@d.co', '?@d.co', 'rame@mail.com', '', null, undefined)
  valueObjects.forEach(elem => {
    const result = t.context.rules.email(elem.value)
    t.is(typeof result, 'boolean', elem.msg)
  })
})

test('email (not satisfied)', t => {
  const valueObjects = generateValues(
    '@',
    '.',
    'a@',
    '@a',
    'a@a.',
    'a@a.c',
    ':a@a.co',
    '.a@a.co',
    'email',
    'e.com',
    'a@a@d.co',
    'd@d.co@',
    '@d@d.co'
  )
  valueObjects.forEach(elem => {
    const result = t.context.rules.email(elem.value)
    t.is(typeof result, 'string', elem.msg)
  })
})

test('percent (satisfied)', t => {
  const valueObjects = generateValues(
    '',
    '100',
    '100.0',
    '100.00',
    '0',
    '0.1',
    '0.00000000',
    '0.12345678',
    '1',
    '1.1',
    '10',
    '10.1',
    '10.12345678'
  )
  valueObjects.forEach(elem => {
    const result = t.context.rules.percent()(elem.value)
    t.is(typeof result, 'boolean', elem.msg)

    const result_2 = t.context.rules.percent({
      min: 1,
      max: 10,
    })(elem.value)
    t.is(typeof result_2, 'boolean', elem.msg)
  })
})

test('percent (not satisfied)', t => {
  const valueObjects = generateValues(
    '1000',
    '1000.',
    '101',
    '101.',
    '100.',
    '100.000',
    '00.',
    '0.',
    '00',
    '00.',
    '00.1',
    '01',
    '01.',
    '015',
    '015.',
    '0.123456789',
    '0.1.',
    '1.',
    '10.',
    '10.123456789',
    '-',
    '-0',
    '-0.1',
    '-0.01',
    '-1',
    '-1.25',
    '-10',
    '-10.25',
    'a',
    '0a',
    '.'
  )
  valueObjects.forEach(elem => {
    const result = t.context.rules.percent()(elem.value)
    t.is(typeof result, 'string', elem.msg)
  })
})

test('percent (not satisfied by length)', t => {
  const valueObjects = generateValues('10.1', '10.1111')
  valueObjects.forEach(elem => {
    const result = t.context.rules.percent({ min: 2, max: 3 })(elem.value)
    t.is(typeof result, 'string', elem.msg)
  })
})

test('amount (satisfied)', t => {
  const valueObjects = generateValues(
    '0',
    '00',
    '00.00',
    '00.25',
    '0.0',
    '0.00',
    '0.25',
    '-0',
    '-00',
    '-0.00',
    '-00.00',
    '-0.01',
    '-00.01',
    '-1',
    '-1.01',
    '-1.0',
    '-100',
    '1',
    '1.0',
    '1.00',
    '1.25',
    '100.0',
    '100.00',
    '100.25'
  )
  valueObjects.forEach(elem => {
    const result = t.context.rules.amount(elem.value)
    t.is(typeof result, 'boolean', elem.msg)
  })
})

test('amount (not satisfied)', t => {
  const valueObjects = generateValues(
    '',
    '-',
    '0.',
    '0.001',
    '00.',
    '01',
    '0.1.',
    '.25',
    '1.',
    '1.100',
    '100.',
    '100.001',
    '10.900',
    '-10.900'
  )
  valueObjects.forEach(elem => {
    const result = t.context.rules.amount(elem.value)
    t.is(typeof result, 'string', elem.msg)
  })
})

test('amountNotNeg (satisfied)', t => {
  const valueObjects = generateValues(
    '0',
    '00',
    '0.0',
    '0.00',
    '00.01',
    '0.01',
    '-0',
    '-00',
    '1',
    '1.01',
    '100',
    '100.0',
    '100.01'
  )
  valueObjects.forEach(elem => {
    const result = t.context.rules.amountNotNeg(elem.value)
    t.is(typeof result, 'boolean', elem.msg)
  })
})

test('amountNotNeg (not satisfied)', t => {
  const valueObjects = generateValues(
    '',
    '-',
    '0.',
    '0.001',
    '00.',
    '01',
    '0.1.',
    '.25',
    '1.',
    '1.100',
    '100.',
    '100.001',
    '10.900',
    '-10.900',
    '-0.1',
    '-1.0',
    '-100',
    '-1.',
    '.'
  )
  valueObjects.forEach(elem => {
    const result = t.context.rules.amountNotNeg(elem.value)
    t.is(typeof result, 'string', elem.msg)
  })
})

test('positive (satisfied)', t => {
  const valueObjects = generateValues('00.01', '0.01', '1', '1.01', '100', '100.0', '100.01')
  valueObjects.forEach(elem => {
    const result = t.context.rules.positive(elem.value)
    t.is(typeof result, 'boolean', elem.msg)
  })
})

test('positive (not satisfied)', t => {
  const valueObjects = generateValues(
    '-',
    '0.',
    '00.',
    '-.25',
    '-1.',
    '-1.100',
    '-100.',
    '-100.001',
    '-10.900',
    '-10.900',
    '-0.1',
    '-1.0',
    '-100',
    '-1.',
    '.'
  )
  valueObjects.forEach(elem => {
    const result = t.context.rules.positive(elem.value)
    t.is(typeof result, 'string', elem.msg)
  })
})

test('latinCode (satisfied)', t => {
  const valueObjects = generateValues('word', 't90est', 'w_ord')
  valueObjects.forEach(elem => {
    const result = t.context.rules.latinCode(elem.value)
    t.is(typeof result, 'boolean', elem.msg)
  })
})

test('latinCode (not satisfied)', t => {
  const valueObjects = generateValues('3word', '_test', '998908', ';?!', 'w;ord')
  valueObjects.forEach(elem => {
    const result = t.context.rules.latinCode(elem.value)
    t.is(typeof result, 'string', elem.msg)
  })
})

test('minLength (satisfied)', t => {
  const valueObjects = generateValues('Georgia', 'Ambro-Soft', null, '')
  valueObjects.forEach(elem => {
    const result = t.context.rules.minLength({
      value: 4,
    })(elem.value)
    t.is(typeof result, 'boolean', elem.msg)
  })
})

test('minLength (not satisfied)', t => {
  const valueObjects = generateValues('Bob', 'a')
  valueObjects.forEach(elem => {
    const result = t.context.rules.minLength({
      value: 4,
    })(elem.value)
    t.is(typeof result, 'string', elem.msg)
  })
})

test('maxLength (satisfied)', t => {
  const valueObjects = generateValues('Georgia', 'AmbroSoft', null, '')
  valueObjects.forEach(elem => {
    const result = t.context.rules.lengthMax({
      value: 10,
    })(elem.value)
    t.is(typeof result, 'boolean', elem.msg)
  })
})

test('maxLength (not satisfied)', t => {
  const valueObjects = generateValues('Bob Dilan is', 'abcd123456789')
  valueObjects.forEach(elem => {
    const result = t.context.rules.lengthMax({
      value: 10,
    })(elem.value)
    t.is(typeof result, 'string', elem.msg)
  })
})

test('compareStrict (satisfied)', t => {
  const valueObjects = generateValues('pass')
  valueObjects.forEach(elem => {
    const result = t.context.rules.compareStrict({
      other: 'pass',
    })(elem.value)
    t.is(typeof result, 'boolean', elem.msg)
  })
})

test('compareStrict (not satisfied)', t => {
  const valueObjects = generateValues('pass')
  valueObjects.forEach(elem => {
    const result = t.context.rules.compareStrict({
      other: '',
    })(elem.value)
    t.is(typeof result, 'string', elem.msg)
  })
})

test('passwordStrength (satisfied)', t => {
  const valueObjects = generateValues('Passw0rd', '1234Task', 'Geo123gia')
  valueObjects.forEach(elem => {
    const result = t.context.rules.passwordStrength(elem.value)
    t.is(typeof result, 'boolean', elem.msg)
  })
})

test('passwordStrength (not satisfied)', t => {
  const valueObjects = generateValues('pass', '12345678', 'geo123gia', 'BIG_PASSWORD')
  valueObjects.forEach(elem => {
    const result = t.context.rules.passwordStrength(elem.value)
    t.is(typeof result, 'string', elem.msg)
  })
})

test('descrip_en_de', t => {
  const valueObjects = generateValues('test testadze')
  valueObjects.forEach(elem => {
    const result = t.context.rules.descrip_en_de(elem.value)
    t.is(typeof result, 'boolean', elem.msg)
  })
})

test('descrip_en_de (not satisfied)', t => {
  const valueObjects = generateValues(
    '  ',
    '\n\n',
    '\t\t',
    '\ntest',
    '\ttest',
    ' test',
    'test\n',
    'test\t',
    'test '
  )
  valueObjects.forEach(elem => {
    const result = t.context.rules.descrip_en_de(elem.value)
    t.is(typeof result, 'string', elem.msg)
  })
})

test('descripEnDe', t => {
  const valueObjects = generateValues('A12b', 't2est Händlerbereich testadze a5', ' test')
  valueObjects.forEach(elem => {
    const result = t.context.rules.descripEnDe(elem.value)
    t.is(typeof result, 'boolean', elem.msg)
  })
})

test('descripEnDe (not satisfied)', t => {
  const valueObjects = generateValues('აბკ')
  valueObjects.forEach(elem => {
    const result = t.context.rules.descripEnDe(elem.value)
    t.is(typeof result, 'string', elem.msg)
  })
})

test('nameEnDe', t => {
  const valueObjects = generateValues('A12b', 't2est Händlerbereich testadze a5', ' test', '', null, undefined)
  valueObjects.forEach(elem => {
    const result = t.context.rules.nameEnDe(elem.value)
    t.is(typeof result, 'boolean', elem.msg)
  })
})

test('nameEnDe (not satisfied)', t => {
  const valueObjects = generateValues('>', 'abcდ')
  valueObjects.forEach(elem => {
    const result = t.context.rules.nameEnDe(elem.value)
    t.is(typeof result, 'string', elem.msg)
  })
})

test('zip', t => {
  const valueObjects = generateValues('12Ab', 'A0101A')
  valueObjects.forEach(elem => {
    const result = t.context.rules.zip(elem.value)
    t.is(typeof result, 'boolean', elem.msg)
  })
})

test('zip (not satisfied)', t => {
  const valueObjects = generateValues('>', 'abcდ')
  valueObjects.forEach(elem => {
    const result = t.context.rules.zip(elem.value)
    t.is(typeof result, 'string', elem.msg)
  })
})

test('link', t => {
  const valueObjects = generateValues('https://a', 'http://b')
  valueObjects.forEach(elem => {
    const result = t.context.rules.link(elem.value)
    t.is(typeof result, 'boolean', elem.msg)
  })
})

test('link (not satisfied)', t => {
  const valueObjects = generateValues('http>', 'abcდ', 'https:/a', 'http://')
  valueObjects.forEach(elem => {
    const result = t.context.rules.link(elem.value)
    t.is(typeof result, 'string', elem.msg)
  })
})

test('enDigitDash', t => {
  const valueObjects = generateValues('A1-2b', 'A0101A')
  valueObjects.forEach(elem => {
    const result = t.context.rules.enDigitDash(elem.value)
    t.is(typeof result, 'boolean', elem.msg)
  })
})

test('enDigitDash (not satisfied)', t => {
  const valueObjects = generateValues('ab/cd', '>', 'abcდ')
  valueObjects.forEach(elem => {
    const result = t.context.rules.enDigitDash(elem.value)
    t.is(typeof result, 'string', elem.msg)
  })
})

test('beginsWithEnDeLetter', t => {
  const valueObjects = generateValues('Aაასა', 'a*/>/A')
  valueObjects.forEach(elem => {
    const result = t.context.rules.beginsWithEnDeLetter(elem.value)
    t.is(typeof result, 'boolean', elem.msg)
  })
})

test('beginsWithEnDeLetter (not satisfied)', t => {
  const valueObjects = generateValues('აab/cd', '>', '<abcდ')
  valueObjects.forEach(elem => {
    const result = t.context.rules.beginsWithEnDeLetter(elem.value)
    t.is(typeof result, 'string', elem.msg)
  })
})

// test('nickname (satisfied)', t => {
//   const valueObjects = generateValues(
//     'a123',
//     'a1b2',
//     'abcdabcdabcdabcd',
//     'ab-cd',
//     'ab_cd',
//     'ab-12-cd',
//     'ab-12_cd',
//     'ab_12-cd',
//     'ab_12_cd',
//     'ab--cd',
//     'ab__cd',
//     'ab-_cd'
//   )
//   valueObjects.forEach(elem => {
//     const result = t.context.rules.nickname(elem.value)
//     t.is(typeof result, 'boolean', elem.msg)
//   })
// })

// test('nickname (not satisfied)', t => {
//   const valueObjects = generateValues(
//     undefined,
//     '',
//     'abc',
//     '1abc',
//     'abcdabcdabcdabcdf',
//     'ab cd',
//     'ab.cd',
//     'ab?cd',
//     'ab=cd',
//     'ab+cd',
//     'ab,cd',
//     'ab!cd',
//     'ab;cd',
//     'ab@cd',
//     'ab#cd',
//     'ab$cd',
//     'ab%cd',
//     'ab^cd',
//     'ab&cd',
//     'ab*cd',
//     'ab(cd',
//     'ab)cd',
//     'ab+cd',
//     'ab=cd',
//     'ab|cd',
//     'ab/cd',
//     'ab\\cd',
//     'cd<cd',
//     'ab>cd'
//   )
//   valueObjects.forEach(elem => {
//     const result = t.context.rules.nickname(elem.value)
//     t.is(typeof result, 'string', elem.msg)
//   })
// })
