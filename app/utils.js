exports.isObjectWithKeys = (param) => {
  return !!param && !Array.isArray(param) && typeof param === 'object' && Object.keys(param).length > 0;
}

exports.validRangeBounds = (min, max) => {
  return  (min == undefined && max == undefined ) ||
          (min == undefined && typeof max == 'number') ||
          (typeof min == 'number' && max == undefined) ||
          ((typeof min == 'number' && typeof max == 'number') && min <= max);
}

exports.isValueInBounds = (value, min, max, minClose, maxClose) => {
  const minimum = (min == undefined) ? -Infinity : min;
  const maximum = (max == undefined) ?  Infinity : max;
  const greaterThenMin  = (minClose) ? value >= minimum : value > minimum;
  const lessThenMax     = (maxClose) ? value <= maximum : value < maximum;
  return greaterThenMin && lessThenMax;
}