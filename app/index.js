const { isObjectWithKeys, validRangeBounds, isValueInBounds } = require('./utils')

function _validate(rule, value, ...restParams) {
  return typeof rule(...restParams)(value) === 'boolean'
}

function _number(pattern, errMsg, options) {
  return value => {
    let validation = pattern.test(value)
    if (validation && isObjectWithKeys(options) && validRangeBounds(options.minVal, options.maxVal)) {
      const interval = options.interval || [true, true]
      validation = validation && isValueInBounds(value, options.minVal, options.maxVal, ...interval)
    }
    return validation || errMsg
  }
}

function _processMinMax(options, defMin, defMax) {
  if (!isObjectWithKeys(options)) {
    options = { min: defMin, max: defMax }
  }
  if (options.min == undefined || options.min <= 0 || (options.max != undefined && options.min > options.max)) {
    options.min = defMin
  }
  if (options.max == undefined || options.max <= 0 || (options.min != undefined && options.min > options.max)) {
    options.max = defMax
  }
  return options
}

// INTEGERs:
function integer(errMsg, options) {
  const pattern = /^-?\d+$/
  return _number(pattern, errMsg, options)
}

function integerWrapper(errMsg) {
  return options => integer(errMsg, options)
}

// Not Negative
function integerNotNeg(errMsg, options) {
  return value => (_validate(integer, value, errMsg, options) && Number(value) >= 0) || errMsg
}

function integerNotNegWrapper(errMsg) {
  return options => integerNotNeg(errMsg, options)
}

// Negative
function integerNeg(errMsg, options) {
  return value => (_validate(integer, value, errMsg, options) && Number(value) < 0) || errMsg
}

function integerNegWrapper(errMsg) {
  return options => integerNeg(errMsg, options)
}

// DOUBLEs:
function double(errMsg, options) {
  const pattern = /^-?\d+(\.\d+)?$/
  return _number(pattern, errMsg, options)
}

function doubleWrapper(errMsg) {
  return options => double(errMsg, options)
}

// Not Negative
function doubleNotNeg(errMsg, options) {
  return value => (_validate(double, value, errMsg, options) && Number(value) >= 0) || errMsg
}

function doubleNotNegWrapper(errMsg) {
  return options => doubleNotNeg(errMsg, options)
}

// Negative
function doubleNeg(errMsg, options) {
  return value => (_validate(double, value, errMsg, options) && Number(value) < 0) || errMsg
}

function doubleNegWrapper(errMsg) {
  return options => doubleNeg(errMsg, options)
}

function positiveNumber(errMsg) {
  return value => !value || +value > 0 || errMsg
}

function minNumber(errMsg, options) {
  return value => !value || +value >= options.value || errMsg
}

function minNumberWrapper(errMsg) {
  return options => minNumber(errMsg, options)
}

function maxNumber(errMsg, options) {
  return value => !value || +value <= options.value || errMsg
}

function maxNumberWrapper(errMsg) {
  return options => maxNumber(errMsg, options)
}

// REQUIREDs:
function requiredField(errMsg) {
  return value => !!value || errMsg
}

function requiredACRecIds(errMsg) {
  return value => typeof value !== 'undefined' || errMsg
}

function requiredMultiAC(errMsg) {
  return value => (value != null && value.length != 0) || errMsg
}

// Email
function email(errMsg) {
  return value => {
    const pattern =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return !value || pattern.test(value) || errMsg
  }
}
// Phone
function phone(errMsg) {
  return value => {
    const pattern = /^[+]?[0-9 ]*$/
    return !value || pattern.test(value) || errMsg
  }
}
// Percent
function percent(errMsg, options) {
  return value => {
    const defMin = 1,
      defMax = 8
    options = _processMinMax(options, defMin, defMax)
    const pattern = new RegExp(`(^100(\\.0{1,2})?$)|(^(0|[1-9]\\d?)(\\.\\d{${options.min},${options.max}})?$)`)
    return !value || pattern.test(value) || errMsg
  }
}

function percentWrapper(errMsg) {
  return options => percent(errMsg, options)
}

// Amount
function amount(errMsg) {
  return value => {
    const pattern = /^-?(0+|[1-9]\d*)(\.\d{1,2})?$/
    return pattern.test(value) || errMsg
  }
}

function amountNotNeg(errMsg) {
  return value => (_validate(amount, value, errMsg) && Number(value) >= 0) || errMsg
}

function latinCode(errMsg) {
  return value => {
    const pattern = /^[A-Za-z]\w*$/
    return pattern.test(value) || errMsg
  }
}

function minLength(errMsg, options) {
  return value => !value || value.length >= options.value || errMsg
}

function minLengthWrapper(errMsg) {
  return options => minLength(errMsg, options)
}

function maxLength(errMsg, options) {
  return value => !value || value.length <= options.value || errMsg
}

function maxLengthWrapper(errMsg) {
  return options => maxLength(errMsg, options)
}

function compare(errMsg, other) {
  return value => value === other || errMsg
}

function compareStrictNewWrapper(errMsg) {
  return options => compare(errMsg, options.other)
}

function passwordStrength(errMsg) {
  return value => {
    const pattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{1,}$/
    return pattern.test(value) || errMsg
  }
}

function descrip_en_de(errMsg) {
  return value => {
    const pattern = new RegExp('^[a-zA-ZäöüßÄÖÜẞ][a-zA-ZäöüßÄÖÜẞ ]+[a-zA-ZäöüßÄÖÜẞ]$')
    return !value || pattern.test(value) || errMsg
  }
}

function descripEnDe(errMsg) {
  return value => {
    const pattern = /^[\wäöüßÄÖÜẞ\-()' !?,"*=#%+&@$£€\\\/]*$/
    return !value || pattern.test(value) || errMsg
  }
}

function onlyDigits(errMsg) {
  return value => {
    const pattern = /^[0-9]*$/
    return !value || pattern.test(value) || errMsg
  }
}

function onlyGeorgianLetters(errMsg) {
  return value => {
    const pattern = /^[ა-ჰ]*$/
    return !value || pattern.test(value) || errMsg
  }
}

function nameEnDe(errMsg) {
  return value => {
    const pattern = /^[a-zA-Z0-9äöüßÄÖÜẞ\-()' ]*$/
    return !value || pattern.test(value) || errMsg
  }
}

function zip(errMsg) {
  return value => {
    const pattern = /^[\w0-9]*$/
    return !value || pattern.test(value) || errMsg
  }
}

function link(errMsg) {
  return value => {
    const pattern = /(https?:\/\/[^\s]+)/
    return !value || pattern.test(value) || errMsg
  }
}

function enDigitDash(errMsg, options) {
  return value => {
    const pattern = /^[\w-]*$/
    return !value || pattern.test(value) || errMsg
  }
}

function beginsWithEnDeLetter(errMsg, options) {
  return value => {
    const pattern = /^[a-zA-ZäöüßÄÖÜẞ]/
    return !value || pattern.test(value) || errMsg
  }
}

module.exports = (
  translateFn = function (key) {
    return key
  },
  translatorKeys = {}
) => {
  const defaultErrMsg = 'validateError'
  return {
    integer: integerWrapper(translateFn(translatorKeys.integer || defaultErrMsg)),
    integerNeg: integerNegWrapper(translateFn(translatorKeys.integerNeg || defaultErrMsg)),
    integerNotNeg: integerNotNegWrapper(translateFn(translatorKeys.integerNotNeg || defaultErrMsg)),
    double: doubleWrapper(translateFn(translatorKeys.double || defaultErrMsg)),
    doubleNeg: doubleNegWrapper(translateFn(translatorKeys.doubleNeg || defaultErrMsg)),
    doubleNotNeg: doubleNotNegWrapper(translateFn(translatorKeys.doubleNotNeg || defaultErrMsg)),
    required: requiredField(translateFn(translatorKeys.required || defaultErrMsg)),
    requiredACRecIds: requiredACRecIds(translateFn(translatorKeys.required || defaultErrMsg)),
    requiredMultiAC: requiredMultiAC(translateFn(translatorKeys.required || defaultErrMsg)),
    email: email(translateFn(translatorKeys.email || defaultErrMsg)),
    phone: phone(translateFn(translatorKeys.phone || defaultErrMsg)),
    percent: percentWrapper(translateFn(translatorKeys.percent || defaultErrMsg)),
    amount: amount(translateFn(translatorKeys.amount || defaultErrMsg)),
    amountNotNeg: amountNotNeg(translateFn(translatorKeys.amountNotNeg || defaultErrMsg)),
    latinCode: latinCode(translateFn(translatorKeys.latinCode || defaultErrMsg)),
    minLength: minLengthWrapper(translateFn(translatorKeys.passwordLength || defaultErrMsg)),
    compareStrict: compareStrictNewWrapper(translateFn(translatorKeys.notEqualToNew || defaultErrMsg)),
    passwordStrength: passwordStrength(translateFn(translatorKeys.passwordNotStrength || defaultErrMsg)),
    descrip_en_de: descrip_en_de(translateFn(translatorKeys.descrip_en_de || defaultErrMsg)),
    // Below added by Murman ambroladze
    lengthMin: minLengthWrapper(translateFn(translatorKeys.lengthMin || defaultErrMsg)),
    lengthMax: maxLengthWrapper(translateFn(translatorKeys.lengthMax || defaultErrMsg)),
    numberMin: minNumberWrapper(translateFn(translatorKeys.numberMin || defaultErrMsg)),
    numberMax: maxNumberWrapper(translateFn(translatorKeys.numberMax || defaultErrMsg)),
    positive: positiveNumber(translateFn(translatorKeys.positive || defaultErrMsg)),
    link: link(translateFn(translatorKeys.link || defaultErrMsg)),
    onlyDigits: onlyDigits(translateFn(translatorKeys.onlyDigits || defaultErrMsg)),
    onlyGeorgianLetters: onlyGeorgianLetters(translateFn(translatorKeys.onlyGeorgianLetters || defaultErrMsg)),
    // Below added by Gia Lomidze and Murman ambroladze
    descripEnDe: descripEnDe(translateFn(translatorKeys.descripEnDe || defaultErrMsg)),
    nameEnDe: nameEnDe(translateFn(translatorKeys.nameEnDe || defaultErrMsg)),
    zip: zip(translateFn(translatorKeys.zip || defaultErrMsg)),
    enDigitDash: enDigitDash(translateFn(translatorKeys.enDigitDash || defaultErrMsg)),
    beginsWithEnDeLetter: beginsWithEnDeLetter(translateFn(translatorKeys.beginsWithEnDeLetter || defaultErrMsg)),
  }
}
